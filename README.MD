## Engineering Webpage
Project description

## Running the project

Install the dependencies with npm:

```bash
npm install
```

Rename `sample.env` to `.env` and replace the following values:

* `ISSUER_BASE_URL` - absolute URL to your Auth0 application domain
* `CLIENT_ID` - your Auth0 application client id
* `SECRET` - a randomly generated string.


#### Run the app:

```bash
npm start
```
#### Or from Docker container:

Create an image:

```bash
docker build -t engineering-webpage .
```

Run it in a container:

```bash
docker run -p 3000:3000 engineering-webpage
```
 
The app will be served at [localhost:3000](http://localhost:3000)
